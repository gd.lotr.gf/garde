import {dropdownInfo} from "./settingDebug";
import {dropdownCreate} from './dropdown'
import {getinfopage} from './infopage'
import {getPerformance} from './performance'
import {getError} from './error'
import {getinfolog} from './infolog'
import {getRoutes} from './listeRoutes'
import {getGlobal} from './global'
import {getRequest} from './request'
import {getPersonalisation} from './personalisation'


const debugBar = document.querySelector('#debug-bar')
if (debugBar){
    let errorjs = []
    window.onerror = function(message, source, lineno, colno, error) {
        errorjs.push({'message' : message, 'source' : source, 'line' : lineno, 'column' : colno, 'error' : error})
    };

    window.onload = function() {
        dropdownCreate()
        getinfopage()
        getPerformance()
        getRoutes()
        getError(errorjs)
        getGlobal()
        getRequest()
        getinfolog()
        getPersonalisation()

        const openDebug = document.querySelector('#debug-bar-open-button')
        const closeDebug = document.querySelector('#debug-bar-close-button')
        const allHead = document.querySelectorAll('.debug-bar-dropdown-head')
        const allDropDown = document.querySelectorAll('.debug-bar-dropdown-content')

////OPEN CLOSE DEBUG
        openDebug.addEventListener('click', function (){
            debugBar.classList.add('debug-bar-open')
        });
        closeDebug.addEventListener('click', function (){
            debugBar.classList.remove('debug-bar-open')
            for (let i=0; i<allDropDown.length; i++){
                allDropDown[i].style.height = '0px'
                allHead[i].classList.remove('debug-bar-dropdown-open')
            }
        });


////OPEN CLOSE DROPDOWN
        function dropDownClick(dropdown, head){
            if (dropdown.clientHeight === 0 || dropdown.clientHeight < dropdown.scrollHeight) {
                for (let i=0; i<allDropDown.length; i++){
                    if (allDropDown[i] !== dropdown){
                        allDropDown[i].style.height = '0px'
                        allHead[i].classList.remove('debug-bar-dropdown-open')
                    }
                }
                dropdown.style.height = dropdown.scrollHeight + 'px';
                head.classList.add('debug-bar-dropdown-open')
            } else {
                dropdown.style.height = '0px';
                head.classList.remove('debug-bar-dropdown-open')
            }
        }

        function setupDropdownClick(head, content) {
            head.addEventListener('click', function (){
                dropDownClick(content, head);
            });
        }

        for (let i = 0; i<allDropDown.length; i++){
            const openDropdown = allHead[i];
            const dropdownContent = allDropDown[i];
            setupDropdownClick(openDropdown, dropdownContent);
        }
    };
}