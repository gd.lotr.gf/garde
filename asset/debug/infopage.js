import {host} from './settingDebug'
function route()
{
    const listeRoutes=document.createElement('ul')
    listeRoutes.id="listeRoute"
    listeRoutes.classList.add('liste-element')
    listeRoutes.innerText='Route:'

    const liURL=document.createElement('li')
    liURL.classList.add("element")
    liURL.innerText='URL: '+window.location.href

    const liChemin=document.createElement('li')
    liChemin.classList.add("element")
    liChemin.innerText='Chemin d\'accès: '+window.location.pathname

    const liDomaine=document.createElement('li')
    liDomaine.classList.add("element")
    liDomaine.innerText='Nom de domaine: '+window.location.hostname

    const liServeur=document.createElement('li')
    liServeur.classList.add("element")
    liServeur.innerText='Nom du serveur: '+window.location.host



    listeRoutes.appendChild(liURL)
    listeRoutes.appendChild(liChemin)
    listeRoutes.appendChild(liDomaine)
    listeRoutes.appendChild(liServeur)
    const boxInfo=document.querySelector('#debug-bar-dropdown-info-content')
    if (boxInfo){
        boxInfo.append(listeRoutes)
    }

}
function css()
{
    const activeCSS = Array.from(document.styleSheets)
        .filter(sheet => sheet.href)
        .map(sheet => sheet.href);

    const listeCSS=document.createElement('ul')
    listeCSS.id="listeCSS"
    listeCSS.classList.add('liste-element')
    listeCSS.innerText='Liste des css actifs:'
    activeCSS.forEach((css)=>
    {
        if(css === host()+'/dist/css/style-debug.bundle.css?version=1.0.0'){
        }else {
            const liCSS = document.createElement('li')
            liCSS.classList.add("element")
            liCSS.innerText = css
            listeCSS.appendChild(liCSS)
        }
    })
    const boxInfo=document.querySelector('#debug-bar-dropdown-info-content')
    if (boxInfo){
        boxInfo.append(listeCSS)
    }
}

function js()
 {
     const activeJS = Array.from(document.scripts)
         .filter(script => script.src)
         .map(script => script.src);

     const listeJS=document.createElement('ul')
     listeJS.id="listeJS"
     listeJS.classList.add('liste-element')
     listeJS.innerText='Liste des JS actifs:'
     activeJS.forEach((js)=>
     {
        if(js === host()+'/dist/js/debug.bundle.js?version=1.0.0'){
        }else{
            const liJS=document.createElement('li')
            liJS.classList.add("element")
            liJS.innerText=js
            listeJS.appendChild(liJS)
        }
     })
     const boxInfo=document.querySelector('#debug-bar-dropdown-info-content')
     if (boxInfo){
         boxInfo.append(listeJS)
     }
 }

export function getinfopage()
{
    route()
    css()
    js()
}


