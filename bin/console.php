<?php

require 'vendor/autoload.php';

use Core\Console\MakeCrud;
use Core\Console\ConsoleModel;

$run = new MakeCrud($argv);
if($argv[1] == 'makecrud') {
    $run->makeCrud($argv[2]);
} else {
    ConsoleModel::processCommands();
}