<?php
namespace App\Service;

class Validation
{
    protected $errors = array();

    public function IsValid($errors)
    {
        foreach ($errors as $key => $value) {
            if(!empty($value)) {
                return false;
            }
        }
        return true;
    }

    /**
     * emailValid
     * @param email $email
     * @return string $error
     */

    public function emailValid($email)
    {
        $error = '';
        if(empty($email) || (filter_var($email, FILTER_VALIDATE_EMAIL)) === false) {
            $error = 'Adresse email invalide.';
        }
        return $error;
    }

    /**
     * textValid
     * @param POST $text string
     * @param title $title string
     * @param min $min int
     * @param max $max int
     * @param empty $empty bool
     * @return string $error
     */

    public function textValid($text, $title, $min = 3,  $max = 50, $empty = true)
    {

        $error = '';
        if(!empty($text)) {
            $strtext = strlen($text);
            if($strtext > $max) {
                $error = 'Votre ' . $title . ' est trop long.';
            } elseif($strtext < $min) {
                $error = 'Votre ' . $title . ' est trop court.';
            }
        } else {
            if($empty) {
                $error = 'Veuillez renseigner un ' . $title . '.';
            }
        }
        return $error;

    }

    public function fileValid ($file, $size_max = 2000000,$goodExtension = array('image/jpeg', 'image/png', 'image/jpg'))
    {
        $error = '';
        if ($file['image']['error'] > 0) {
            if ($file['image']['error'] != 4) {
                $error = 'Problem: ' . $file['image']['error'] . '<br />';
            }else {
                $error = 'Aucun fichier n\'a été téléchargé';
            }
        }else{
            $file_size = $_FILES['image']['size'];
            $file_tmp  = $_FILES['image']['tmp_name'];

            if($file_size > $size_max || filesize($file_tmp) > $size_max) {
                $error = 'Le fichier est trop gros (max '.$size_max.' octets)';
            } else {
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mime = finfo_file($finfo, $file_tmp);
                finfo_close($finfo);

                if (!in_array($mime, $goodExtension)) {
                    $str = '';
                    foreach($goodExtension as $ext) {
                        $str .= $ext . ',';
                    }
                    $error = 'Veuillez télécharger une image de type ' .substr($str, 0, -1);
                }
            }
        }
        return $error;
    }


}
