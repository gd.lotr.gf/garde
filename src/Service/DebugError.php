<?php

namespace App\Service;

class DebugError
{

    public function getError()
    {

        $config = include  dirname(__DIR__) . '../../config/config-dist.php';
        if ($config['mode'] == 'dev'){
            $_SESSION['debugpost'] = $_POST;
            if (!isset($_COOKIE['verif_set_error'])) {
                unset($_SESSION['errors']);

                ini_set('display_errors', 1);
                ini_set('display_startup_errors', 1);
                error_reporting(E_ALL);

                setcookie('verif_set_error', 'actif', time() + 1, "/");
                set_error_handler([$this, "errorHandler"]);
            }
        }
    }

    public function errorHandler($errno, $errstr, $errfile, $errline)
    {
        $_SESSION['errors'][] = array(
            'errno'   => $errno,
            'errstr'  => $errstr,
            'errfile' => $errfile,
            'errline' => $errline
        );
    }

}