<?php

namespace App\Controller;

use App\Model\$NameModel;
use App\Service\Form;
use App\Service\Validation;

class $NameController extends BaseController
{

    public function index()
    {
        $message = 'Ok ça marche';
        $items = $NameModel::all();
        $this->render('app.$name.index', array(
            'message' => $message,
            'items' => $items,
        ), 'base');
    }

    public function add()
{
    $message = 'Ok ça marche';
    $errors = [];
    if(!empty($_POST['submitted'])) {
        $post = $this->cleanXss($_POST);
        $validate = new Validation();
        $errors = $this->validate($validate,$post);
        if($validate->IsValid($errors)) {
            $nameModel::insert($post);
            $this->addFlash('success', 'Formulaire fonctionnel');
            $this->redirect('$name');
        }
    }
    $form = new Form($errors);
    $this->render('app.$name.add', array(
    'message' => $message,
    'form' => $form,
), 'base');
    }


    public function single($id)
{
    $message = 'Ok ça marche';
    $item = $this->getItemByIdOr404($id);
    $this->render('app.$name.single', array(
    'message' => $message,
    'item' => $item,
), 'base');
    }


    public function edit($id)
{
    $message = 'Ok ça marche';
    $item = $this->getItemByIdOr404($id);
    $errors = [];
    if(!empty($_POST['submitted'])) {
        $post = $this->cleanXss($_POST);
        $validate = new Validation();
        $errors = $this->validate($validate,$post);
        if($validate->IsValid($errors)) {
            $NameModel::update($post,$id);
            $this->addFlash('success', 'Item Modifié');
            $this->redirect('$name');
        }
    }
    $form = new Form($errors);
    $this->render('app.$name.edit', array(
    'message' => $message,
    'item' => $item,
    'form' => $form,
), 'base');
    }

    private function validate($validate,$post)
{
    $errors['item'] = $validate->textValid($post['item'], 'item', 2, 70);
    return $errors;
}

    private function getItemByIdOr404($id)
{
    $item = $NameModel::findById($id);
    if(empty($item)) {
        $this->Abort404();
    }
    return $item;
}

}