<?php

//MakeCrud.php
namespace Core\Console;


use Core\App;
use Core\Kernel\Database;

class MakeCrud
{
    private $argv;
    public $index = '<h2><?=$message?></h2>';
    public $add = '<h2><?=$message?></h2>';
    public $edit = '<h2><?=$message?></h2>';
    public $single = '<h2><?=$message?></h2>';

    public function __construct($argv)
    {
        $this->argv = $argv;
    }

    private function TextController($name) {
        if (!file_exists('src/Controller/BaseController.php')){
            $textBaseController = file_get_contents('core/Console/TextBaseController.php');
            file_put_contents('src/Controller/BaseController.php',$textBaseController);
        }
        $textController = file_get_contents('core/Console/TextController.php');
        $textController = str_replace('$Name', $name, $textController);
        $textController = str_replace('$name', strtolower($name), $textController);
        return $textController;
    }

    private function TextModel($name){
        $textModel = file_get_contents('core/Console/TextModel.php');
        $textModel = str_replace('$Name', $name, $textModel);
        $textModel = str_replace('$name', strtolower($name), $textModel);
        return $textModel;
    }

    private function index($name){
        $index = file_get_contents('core/Console/index.php');
        $index = str_replace('$name', strtolower($name), $index);
        return $index;
    }

    private function add($name){
        $add = file_get_contents('core/Console/add.php');
        $add = str_replace('$name', strtolower($name), $add);
        return $add;
    }

    private function edit($name){
        $edit = file_get_contents('core/Console/edit.php');
        $edit = str_replace('$name', strtolower($name), $edit);
        return $edit;
    }

    private function single($name){
        $single = file_get_contents('core/Console/single.php');
        $single = str_replace('$name', strtolower($name), $single);
        return $single;
    }

    private function addRoute($name) {
        $routeFile = 'config/routes.php';

        $fileContent = file_get_contents($routeFile);

        $newContent = "\n// CRUD $name\n";

        $name = strtolower($name);

        $newContent .= implode("\n", [
            "    array('$name','$name','index'),",
            "    array('$name"."_add','$name','add'),",
            "    array('$name"."_edit','$name','edit',array('id')),",
            "    array('$name"."_single','$name','single',array('id')),",
        ]);

        $fileContent = preg_replace(
            '/\);/',
            $newContent . "\n\n);",
            $fileContent,
            1
        );

        file_put_contents($routeFile, $fileContent);
    }

    public function makeCrud($name = 'Noname')
    {
        file_put_contents('src/Controller/'.$name.'Controller.php',$this->TextController($name));
        file_put_contents('src/Model/'.$name.'Model.php',$this->TextModel($name));
        mkdir('template/app/'.strtolower($name));
        file_put_contents('template/app/'.strtolower($name).'/index.php',$this->index($name));
        file_put_contents('template/app/'.strtolower($name).'/add.php',$this->add($name));
        file_put_contents('template/app/'.strtolower($name).'/edit.php',$this->edit($name));
        file_put_contents('template/app/'.strtolower($name).'/single.php',$this->single($name));
        $this->addRoute($name);
    }

}