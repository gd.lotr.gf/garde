<?php

namespace App\Model;
use Core\App;
use Core\Kernel\AbstractModel;
class $NameModel extends AbstractModel
{

    protected $id;
    protected $item;

    /**
     * @return mixed
     */
    public function getId()
{
    return $this->id;
}

    /**
     * @return mixed
     */
    public function getItem()
{
    return $this->item;
}

    protected static $table = '$name';

    public static function insert($post) {
    App::getDatabase()->prepareInsert(
        "INSERT INTO " . self::$table . " (item) VALUES (?)",
        [$post['item']]
    );
}

    public static function update($post,$id) {
    App::getDatabase()->prepareInsert(
        "UPDATE " . self::$table . " SET item = ? WHERE id = ?",
        [$post['item'], $id]
    );
}

}